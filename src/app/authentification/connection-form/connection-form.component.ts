import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import {Router} from "@angular/router"

@Component({
  selector: 'app-connection-form',
  templateUrl: './connection-form.component.html',
  styleUrls: ['./connection-form.component.scss']
})
export class ConnectionFormComponent implements OnInit {

  constructor(private authentificationService: AuthentificationService, private router: Router){ }

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  error: string;
  
  ngOnInit() {
  }

  submit(){
    this.authentificationService.login(this.form.value.username, this.form.value.password)
    .pipe(
      catchError(err => {
        console.log("enter");
        this.error = 'Email ou mot de passe incorrect.';
        return of(err);
      })
    )
    .subscribe(result => {
      sessionStorage.setItem('token', result.access_token);
      window.location.replace("http://localhost:4200/pokemons");
    });
  }

}
