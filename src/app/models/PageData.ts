import { Pokemon } from './Pokemon';

export interface PageData {
    data: Pokemon[];
    limit: number;
    offset: number;
}