import { Component, OnInit, Output, Input} from '@angular/core';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent implements OnInit {

  pokemonIdSelected: number = 1;

  teamSize: number = -1;

  pokemonSelectedToAddToMyTeam: number = -1;

  pokemonAddedReccurence: number = 0;

  constructor() { }

  ngOnInit(){
  }

  changePokemonDisplay(id : number ){
    console.log(id);
    this.pokemonIdSelected = id;
  }

  changeTeamSize(id: number){
    this.teamSize = id;
  }

  addPokemonToMyTeam(id: number){
    this.pokemonSelectedToAddToMyTeam = id;
  }

  addSamePokemonToMyTeam(reccurence: number){
    this.pokemonAddedReccurence = reccurence;
  }
}
