import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/models/Pokemon';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnChanges {

  pokemon: Pokemon; 

  @Input() teamSize : number;

  @Input() pokemonIdSelected: number = 1;

  @Output() pokemonSelectedToAddToMyTeam: EventEmitter<number> = new EventEmitter();

  @Output() pokemonAddToTeamRecurrence: EventEmitter<number> = new EventEmitter();

  private lastPokemonAddedToTeam: number = -1;

  private reccurence: number = 0;

  constructor(private route: ActivatedRoute, private pokemonService: PokemonService) { }

  ngOnChanges() {
    this.getPokemon(this.pokemonIdSelected);
  }

  getPokemon(id: number){
    this.pokemonService.getPokemeonById(id).subscribe(result => this.pokemon = result);
  }

  playAudio(id){
    let audio = new Audio();
    audio.src = '../../../assets/audio/' + id + '.mp3';
    audio.load();
    audio.play();
  }

  addPokemonToMyTeam(){
    if (this.lastPokemonAddedToTeam !== this.pokemon.id) {
      this.pokemonSelectedToAddToMyTeam.emit(this.pokemon.id);
    } else {
      this.reccurence += 1;
      this.pokemonAddToTeamRecurrence.emit(this.reccurence);
    }
    this.lastPokemonAddedToTeam = this.pokemon.id;
  }
}
