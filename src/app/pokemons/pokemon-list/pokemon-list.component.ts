import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Pokemon } from 'src/app/models/Pokemon';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss'],
})
export class PokemonListComponent implements OnInit {

  pokemons: Pokemon[];

  currentIdStart: number;

  private searchTerms = new Subject<string>();

  @Output() pokemonIdSelected: EventEmitter<number> = new EventEmitter();

  constructor(private pokemonService: PokemonService) { }

  range = 20;

  searchValue: string;

  ngOnInit() {
    this.getPokemons();
  }

  getPokemons(){
    this.pokemonService.getPokemons(0, this.range).subscribe(result => this.pokemons = result.data);
  }

  getPokemonsSearch() {
    if (this.searchValue === '') {
      this.getPokemons();
    } else {
      this.pokemonService.getPokemonsSearch(0, this.range, this.searchValue ).subscribe(result => this.pokemons = result.data);
    }
  }

  onScroll() {
    this.range += 10;
    this.getPokemons();
  }

  changePokemonDisplay(id){
    this.pokemonIdSelected.emit(id);
  }

  search(term: string){
    this.searchTerms.next(term);
  }

  clear(){
    this.getPokemons();
    this.searchValue = '';
  }
}
