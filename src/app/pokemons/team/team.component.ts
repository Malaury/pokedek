import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Pokemon } from 'src/app/models/Pokemon';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit, OnChanges {

  private team: Pokemon[];
  private listId: number[];
  private cpt: number;
  private badPok: Pokemon;

  @Output() teamSize: EventEmitter<number> = new EventEmitter();

  @Input() pokemonSelectedToAddToMyTeam: number = -1;

  @Input() pokemonAddedReccurence: number = 0;

  constructor(private pokemonService: PokemonService, private teamService: TeamService) { 
    this.team = [];
  }

  ngOnInit() {
    this.getTeam();
  }

  ngOnChanges(){
    if(this.pokemonSelectedToAddToMyTeam !== -1 && this.listId.length < 6){
      this.listId[this.listId.length] = this.pokemonSelectedToAddToMyTeam;
      this.setTeam();
    }
  }

  getTeam(){
    if(!sessionStorage.getItem('token')){
      return;
    }
    this.team = [];
    this.cpt = 0;
    this.teamService.getMyteam().subscribe(result => {
      this.listId = result;
      result.forEach(id => {
        this.pokemonService.getPokemeonById(id).subscribe(pok => {
          this.team[this.cpt] = pok;
          this.cpt++;
          this.changeTeamSize();
        });
      });
    });
  }

  removePokemon(id: number){
    this.listId.splice(this.listId.indexOf(id), 1);
    console.log(this.setTeam());
  }

  setTeam(){
    this.teamService.setMyTeam(this.listId).subscribe(result => {
      this.getTeam();
    });
  }

  changeTeamSize(){
    this.teamSize.emit(this.listId.length);
  }
}
