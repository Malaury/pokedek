import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TokenData } from '../models/tokenData';
import { Observable, pipe, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Token } from '@angular/compiler/src/ml_parser/lexer';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  private pokemonURL = 'http://app-ec21e68e-3e55-42d7-b1ae-3eef7507a353.cleverapps.io';

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<TokenData>{
    return this.http.post<TokenData>(this.pokemonURL + '/auth/login', {email, password})
      .pipe(
        catchError(this.handleError<TokenData>('getLogin'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return throwError(result as T);
    };
  }
}
