import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Pokemon } from '../models/Pokemon';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators'; 
import { PageData } from '../models/PageData';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private pokemonURL = 'http://app-ec21e68e-3e55-42d7-b1ae-3eef7507a353.cleverapps.io';

  constructor(private http: HttpClient) { }

  getPokemons(startId: number, range: number): Observable<PageData>{
    return this.http.get<PageData>(this.pokemonURL + '/pokemons?offset=' + startId + '&limit=' + range)
      .pipe(
        catchError(this.handleError<PageData>('getPokemons'))
      );
  }

  getPokemonsSearch(startId: number, range: number, term: string): Observable<PageData>{
    return this.http.get<PageData>(this.pokemonURL + '/pokemons?offset=' + startId + '&limit=' + range + '&search=' + term)
      .pipe(
        catchError(this.handleError<PageData>('getPokemons'))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getPokemeonById(id: number): Observable<Pokemon>{
    return this.http.get<Pokemon>(this.pokemonURL + '/pokemons/' + id)
      .pipe(
        catchError(this.handleError<Pokemon>('getPokemonById'))
      );
  }
}
