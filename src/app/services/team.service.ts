import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private pokemonURL = 'http://app-ec21e68e-3e55-42d7-b1ae-3eef7507a353.cleverapps.io';

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('token')
    })
  }

  getMyteam(): Observable<number[]> {
    return this.http.get<number[]>(this.pokemonURL + '/trainers/me/team', this.httpOptions)
      .pipe(
        catchError(this.handleError<number[]>('getMyTeam'))
      );
  }

  setMyTeam(list: number[]): Observable<number[]> {
    return this.http.put<number[]>(this.pokemonURL + '/trainers/me/team', list, this.httpOptions)
      .pipe(
        catchError(this.handleError<number[]>('setMyTeam'))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return throwError(result as T);
    };
  }
}
